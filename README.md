Wordpress Development Docker Container
=================================


Installation
------------

1. Install Docker: [Mac](https://docs.docker.com/docker-for-mac/install/)

1. Clone this repository

1. Run docker-compose up:  
`docker-compose up --build`

    or to run in background:  
`docker-compose up -d --build`

XDebug in VSCode
----------------
1. Install [PHP Debug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug) extension in VSCode.

1. Start Debugging (F5)

1. Add some breakpoints to the PHP code. 

1. Load PHP.
