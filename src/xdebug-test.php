<?php

$foo = 'Hello';
$bar = 'World';

$parts = array();
$parts[] = $foo;
$parts[] = $bar;
$parts[] = '!';

echo join(' ', $parts);
